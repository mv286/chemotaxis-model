close all;
clear;


%% 
params.Sstates = [-1, 1];
params.Tstates = [-1, 1];
params.dim     = 2;
params.v  = 10;
params.deltat  = .1;
params.epsilon = 1;

params.B = 5;
params.h0 = 1.1;
params.K = @ (x) (x+1)/2*10 - (x-1)/2*(.1);%1st term coef change from 10 to 1

params.nparticles = 1e5;

%% number of timepoints
params.N  =100;
params.reportPeriod = 100;

%% chemical profile paraneters

params.flank = 4*params.N;
params.gap_between_triangles = 4; 
params.slope_size = 101;
params.Xl = 2*params.flank + params.gap_between_triangles + 4*(params.slope_size) + 1;
params.grad0  = 0.005;
params.grad20 = 0.01;
params.initialPosition =  4*params.N + params.slope_size + params.gap_between_triangles/2;
%% 


particles = particleFilter_bacterial_chemotaxis_model(params);
