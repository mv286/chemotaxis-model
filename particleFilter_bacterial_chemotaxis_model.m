% This is a function that takes the following parameters:
% -  params: structure containing various parameters
%       params.N: number of timepoints
%       params.nparticles: number of particles
%       params.dim: dimension of particle state (=2)
%       params.initialPosition: initial positions for each particle
%       params.reportPeriod: interval for saving the state of the particle
%       population
%       params.B: model parameter B
%       params.h0: model parameter h_0
%       params.K: model parameter K
%       params.v: model parameter v
%       params.deltat: model parameter \Delta t
% - init (optional): Vector with initial values of the state variables.
% - init_w (optional): Vector with initial values of the state variables.
% The function returns the value of the marginal log-likelihood
function [ particles, weights,  Xsample, ic, marginalLogLikelihood, N_eff] = ...
    particleFilter_bacterial_chemotaxis_model(params, init, init_w) 

    timepoints   = params.N;
    nparticles   = params.nparticles;
    particles    = nan(timepoints, nparticles, params.dim);
    weights      = nan(timepoints, nparticles);
    N_eff        = nan(timepoints,1);
    ic           = nan(nparticles,1);
    icTmp        = nan(nparticles,1);
    
    kp =1;
    Xsample = struct();
    %% Initialisation of the algorithm
    % Initialise the state and the weight of your particles
    
    if nargin == 1
        particles(1, :, :) = initialise_1d(params, nparticles) ;
        weights(1, :) = 1/nparticles;
        start_tp = 2;
    else
        particles(1:size(init,1), :, :) = init; %initialise_1d(params, nparticles) ;
        weights(1:size(init_w,1), :) = init_w; %1/nparticles;
        start_tp = size(init_w,1)+1;
    end
    
    ic(:) = params.initialPosition;
    %% Start for loop over timepoints
    
    %normalise weigths & calculate N_eff
    W = weights(1, :) / sum(weights(1, :));
    
    
    % loop through the time-points
    for ti = start_tp:timepoints
        disp([' --- SMC at timepoint ' num2str(ti)])
                
        % Re-sample particles according to their weights
        ancestral_ids = randsample(nparticles, nparticles, true, W);
        
       
        %% Start for() loop over particles
        particlesTmp = particles(:,ancestral_ids, :);
        weightsTmp   = weights(:, ancestral_ids);
        icTmp = ic(ancestral_ids);
        %particlesTmp = particles(:,:, :);
        
        %for each particle
        for p = 1:1:nparticles
            % get state at previous time-point
            X0    = particlesTmp(ti-1, p, :); 
            
            %perturb the state
            Xn    = X0;
            rr    = rand;
            if rr <= 1/3
                Xn(1,1,1) = -1*X0(1,1,1);
                pp = 1/3;
            elseif rr <= 2/3
                Xn(1,1,2) = -1*X0(1,1,2);
                pp=1/3;
            else
                pp=1/3;
            end
            
            % set this as the new curret state
            particlesTmp(ti, p, :) = Xn;

            % Weight the particle 
            a_tmp = particlesTmp(1:ti, p, :);
            DE = energyChange(a_tmp, params, icTmp(p));
            weightsTmp(ti, p) = exp( - DE )/pp;
            
        end
        %% End for() loop over particles
        %update particle matrix 
        particles = particlesTmp;
        ic        = icTmp;
        weights   = weightsTmp;
        
        %%normalise weigths & calculate N_eff
        W = weights(ti, :) / sum(weights(ti, :));
        N_eff(ti) =  nparticles./(1 + nparticles*var(W)/sum(W).^2);
        
        if (mod(ti,params.reportPeriod) == 0 || ti == timepoints ) 
            ind = randsample(nparticles, nparticles, true, weights(ti, :));
            Xsample(kp).population = particles(1:ti,ind, :);
            Xsample(kp).ic         = ic(ind);
            kp=kp+1;
        end
        
    end
    %% End for() loop over observation times
    
    %% Compute and return the marginal log-likelihood
    % sum of the log of the mean of the weights at each observation time
    logc = log(mean(weights,2));
    marginalLogLikelihood = sum(logc(2:end));
   
    
    
       
end


%% random intialisation of the population state
function p = initialise_1d(params, nparticles) 
    
 
    p         =  ones(1, nparticles, 2);
   
    p(1,:, 1) = params.Sstates(randi([1,2], 1, nparticles,1));
    p(1,:, 2) = params.Tstates(randi([1,2], 1, nparticles,1));
    
    
    
end

function r = energyChange(particle, params, ic)
    
    
    i = size(particle,1); 
    S = particle(:,:, 1);
    T = particle(:,:, 2);
    
    epsilon = params.epsilon;
    B = params.B;
    h0 = params.h0;
    K = params.K;
    v = params.v;
    DT = params.deltat;
    
    %calculate current position
    x  = ic + v*DT*sum(particle(:, 1 , 2) .* (particle(:, 1 , 1) + 1)/2);

    
    grad =0;
    % calculate the gradient at position x (a double triangle profile is assumed here)
    if ( x <= (params.flank+params.slope_size-1))
        grad = params.grad0;
    elseif (x > (params.flank+params.slope_size-1) && x <= (params.flank+2*params.slope_size-2))
        grad = -params.grad0;
    elseif (x >=(params.flank+2*params.slope_size+params.gap_between_triangles) && x <= (params.flank+3*params.slope_size+params.gap_between_triangles-2))
        grad = params.grad20;
    elseif (x >= (params.flank+3*params.slope_size+params.gap_between_triangles-1) )
        grad = -params.grad20;
    end
        
    %calculate change
    val(1) = (epsilon/2          ) * ( 1 - S(i-1)*S(i) ) * (1 - S(i)) ...
           + (epsilon/2          ) * ( 1 - S(i-1)*S(i) ) * (1 + S(i));
    val(2) = - (h0/2  +B*((T(i-1))*grad)/2)  * ( S(i) + 1 ) ;
    val(3) = K(S(i-1)) * (1 - T(i-1)*T(i));
    
    r = sum(val);
end

